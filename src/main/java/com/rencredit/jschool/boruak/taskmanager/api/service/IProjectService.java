package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

}
