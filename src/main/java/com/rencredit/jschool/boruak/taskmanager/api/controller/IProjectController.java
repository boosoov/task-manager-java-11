package com.rencredit.jschool.boruak.taskmanager.api.controller;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

}
