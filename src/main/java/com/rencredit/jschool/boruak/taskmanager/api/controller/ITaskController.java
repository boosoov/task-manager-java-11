package com.rencredit.jschool.boruak.taskmanager.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

}
