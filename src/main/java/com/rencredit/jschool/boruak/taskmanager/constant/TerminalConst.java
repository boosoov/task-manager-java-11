package com.rencredit.jschool.boruak.taskmanager.constant;

public interface TerminalConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String EXIT = "exit";

    String INFO = "info";

    String COMMANDS = "commands";

    String ARGUMENTS = "arguments";

    String TASK_CREATE = "task-create";

    String TASK_CLEAR = "task-clear";

    String TASK_LIST = "task-list";

    String PROJECT_CREATE = "project-create";

    String PROJECT_CLEAR = "project-clear";

    String PROJECT_LIST = "project-list";

}
