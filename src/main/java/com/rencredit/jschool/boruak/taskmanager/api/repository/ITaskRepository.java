package com.rencredit.jschool.boruak.taskmanager.api.repository;

import com.rencredit.jschool.boruak.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

}
